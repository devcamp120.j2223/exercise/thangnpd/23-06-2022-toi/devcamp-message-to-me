import "bootstrap/dist/css/bootstrap.min.css";
import backgroundImg from "./assets/images/background.jpg";
import likeImg from "./assets/images/like.png";


function App() {
  return (
    <div className="container text-center">
      <div className="row mt-5">
        <div className="col-12">
          <h1>Chào mừng đến với Devcamp 120</h1>
        </div>
      </div>
      <div className="row mt-2">
        <div className="col-12">
          <img src={backgroundImg} alt="title" width={500} className="img-thumbnail"/>
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <label className="form-label">Message cho bạn 12 tháng tới:</label>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <input className="form-control" placeholder="Nhập message"/>
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <button className="btn btn-success">Gửi thông điệp</button>
        </div>
      </div>
      <div className="row mt-4">
        <div className="col-12">
          <p>Thông điệp ở đây</p>
        </div>
      </div>
      <div className="row mt-2">
        <div className="col-12">
          <img src={likeImg} alt="like" width={100}/>
        </div>
      </div>
    </div>

  );
}

export default App;
